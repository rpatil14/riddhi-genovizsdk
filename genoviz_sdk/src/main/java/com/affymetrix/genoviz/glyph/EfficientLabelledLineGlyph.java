/**
 * Copyright (c) 2001-2007 Affymetrix, Inc.
 *
 * Licensed under the Common Public License, Version 1.0 (the "License"). A copy
 * of the license must be included with any distribution of this source code.
 * Distributions from Affymetrix, Inc., place this in the IGB_LICENSE.html file.
 *
 * The license is also available at http://www.opensource.org/licenses/cpl.php
 */
package com.affymetrix.genoviz.glyph;

import com.affymetrix.genoviz.bioviews.GlyphI;
import com.affymetrix.genoviz.bioviews.ViewI;
import com.affymetrix.genoviz.util.NeoConstants;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A subclass of EfficientLabelledGlyph that makes all its children center
 * themselves vertically on the same line.
 */
public class EfficientLabelledLineGlyph extends EfficientLabelledGlyph {

    public static boolean AUTO_SIZE_LABELS = true;
    public static boolean DYNAMICALLY_SIZE_LABELS = false;
    public static Font OVERRIDE_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 8);
    private boolean move_children = true;
    private static final int maxFontSize = 36;
    private static final int minFontSize = 7;

    private static final Map<Integer, Font> fontSizeReference = new HashMap<Integer, Font>();

    static {
        fontSizeReference.put(7, new Font(Font.MONOSPACED, Font.PLAIN, 9));
        fontSizeReference.put(8, new Font(Font.MONOSPACED, Font.PLAIN, 10));
        fontSizeReference.put(9, new Font(Font.MONOSPACED, Font.PLAIN, 11));
        fontSizeReference.put(10, new Font(Font.MONOSPACED, Font.PLAIN, 12));
        fontSizeReference.put(11, new Font(Font.MONOSPACED, Font.PLAIN, 14));
        fontSizeReference.put(12, new Font(Font.MONOSPACED, Font.PLAIN, 14));
        fontSizeReference.put(13, new Font(Font.MONOSPACED, Font.PLAIN, 15));
        fontSizeReference.put(14, new Font(Font.MONOSPACED, Font.PLAIN, 17));
        fontSizeReference.put(15, new Font(Font.MONOSPACED, Font.PLAIN, 18));
        fontSizeReference.put(16, new Font(Font.MONOSPACED, Font.PLAIN, 19));
        fontSizeReference.put(17, new Font(Font.MONOSPACED, Font.PLAIN, 21));
        fontSizeReference.put(18, new Font(Font.MONOSPACED, Font.PLAIN, 21));
        fontSizeReference.put(19, new Font(Font.MONOSPACED, Font.PLAIN, 22));
        fontSizeReference.put(20, new Font(Font.MONOSPACED, Font.PLAIN, 23));
        fontSizeReference.put(21, new Font(Font.MONOSPACED, Font.PLAIN, 25));
        fontSizeReference.put(22, new Font(Font.MONOSPACED, Font.PLAIN, 26));
        fontSizeReference.put(23, new Font(Font.MONOSPACED, Font.PLAIN, 27));
        fontSizeReference.put(24, new Font(Font.MONOSPACED, Font.PLAIN, 28));
        fontSizeReference.put(25, new Font(Font.MONOSPACED, Font.PLAIN, 29));
        fontSizeReference.put(26, new Font(Font.MONOSPACED, Font.PLAIN, 30));
        fontSizeReference.put(27, new Font(Font.MONOSPACED, Font.PLAIN, 32));
        fontSizeReference.put(28, new Font(Font.MONOSPACED, Font.PLAIN, 33));
        fontSizeReference.put(29, new Font(Font.MONOSPACED, Font.PLAIN, 34));
        fontSizeReference.put(30, new Font(Font.MONOSPACED, Font.PLAIN, 34));
        fontSizeReference.put(31, new Font(Font.MONOSPACED, Font.PLAIN, 36));
        fontSizeReference.put(32, new Font(Font.MONOSPACED, Font.PLAIN, 37));
        fontSizeReference.put(33, new Font(Font.MONOSPACED, Font.PLAIN, 38));
        fontSizeReference.put(34, new Font(Font.MONOSPACED, Font.PLAIN, 40));
        fontSizeReference.put(35, new Font(Font.MONOSPACED, Font.PLAIN, 41));
        fontSizeReference.put(36, new Font(Font.MONOSPACED, Font.PLAIN, 41));
        fontSizeReference.put(37, new Font(Font.MONOSPACED, Font.PLAIN, 43));
        fontSizeReference.put(38, new Font(Font.MONOSPACED, Font.PLAIN, 44));
        fontSizeReference.put(39, new Font(Font.MONOSPACED, Font.PLAIN, 45));
        fontSizeReference.put(40, new Font(Font.MONOSPACED, Font.PLAIN, 46));
        fontSizeReference.put(41, new Font(Font.MONOSPACED, Font.PLAIN, 48));
        fontSizeReference.put(42, new Font(Font.MONOSPACED, Font.PLAIN, 48));
        fontSizeReference.put(43, new Font(Font.MONOSPACED, Font.PLAIN, 49));
        fontSizeReference.put(44, new Font(Font.MONOSPACED, Font.PLAIN, 51));
    }

    @Override
    public void draw(ViewI view) {
        Rectangle2D.Double full_view_cbox = view.getFullView().getCoordBox();
        Graphics g = view.getGraphics();

        // perform an intersection of the view and this glyph, in the X axis only.
        Rectangle2D.Double cbox = this.getCoordBox();
        scratch_cbox.x = Math.max(cbox.x, full_view_cbox.x);
        scratch_cbox.width = Math.min(cbox.x + cbox.width, full_view_cbox.x + full_view_cbox.width) - scratch_cbox.x;
        scratch_cbox.y = cbox.y;
        scratch_cbox.height = cbox.height;

        Rectangle pixelbox = view.getScratchPixBox();
        view.transformToPixels(scratch_cbox, pixelbox);

        int originalPixWidth = pixelbox.width;
        if (pixelbox.width == 0) {
            pixelbox.width = 1;
        }
        if (pixelbox.height == 0) {
            pixelbox.height = 1;
        }

        g.setColor(getBackgroundColor());
        if (show_label) {
            if (getChildCount() <= 0) {
                //        fillDraw(view);
                if (label_loc == NORTH) {
                    g.fillRect(pixelbox.x, pixelbox.y + (pixelbox.height / 2),
                            pixelbox.width, Math.max(1, pixelbox.height / 2));
                } else {
                    g.fillRect(pixelbox.x, pixelbox.y,
                            pixelbox.width, Math.max(1, pixelbox.height / 2));
                }
            } else {
                // draw the line
                if (label_loc == NORTH) { // label occupies upper half, so center line in lower half
                    drawDirectedLine(g, pixelbox.x, pixelbox.y + ((3 * pixelbox.height) / 4),
                            pixelbox.width, pixelbox.height < minHeight ? NeoConstants.NONE : direction);
                } else if (label_loc == SOUTH) {  // label occupies lower half, so center line in upper half
                    drawDirectedLine(g, pixelbox.x, pixelbox.y + (pixelbox.height / 4),
                            pixelbox.width, pixelbox.height < minHeight ? NeoConstants.NONE : direction);
                }
            }

            if (label != null && (label.length() > 0)) {
                if (DYNAMICALLY_SIZE_LABELS) {
                    drawClassicLabel(originalPixWidth, pixelbox, g);
                } else {
                    drawLabel(pixelbox, g);
                }
            }
        } else { // show_label = false, so center line within entire pixelbox
            if (getChildCount() <= 0) {
                // if no children, draw a box
                g.fillRect(pixelbox.x, pixelbox.y + (pixelbox.height / 2),
                        pixelbox.width, Math.max(1, pixelbox.height / 2));
            } else {
                // if there are children, draw a line.
                drawDirectedLine(g, pixelbox.x, pixelbox.y + pixelbox.height / 2,
                        pixelbox.width, pixelbox.height < minHeight ? NeoConstants.NONE : direction);
            }
        }

    }

    private void drawLabel(Rectangle pixelbox, Graphics g) {
        Graphics g2 = g;
        String drawLabel = label;
        int labelGap = pixelbox.height / 4;
        int ypixPerChar = ((pixelbox.height / 2) - labelGap);

        if (ypixPerChar > maxFontSize) {
            ypixPerChar = maxFontSize;
        }
        if (ypixPerChar < minFontSize) {
            ypixPerChar = minFontSize;
        }
        if (AUTO_SIZE_LABELS) {
            g2.setFont(fontSizeReference.get(ypixPerChar));
        } else {
            g2.setFont(OVERRIDE_FONT);
        }
        FontMetrics fm = g2.getFontMetrics();
        int textWidth = fm.stringWidth(drawLabel);
        while (textWidth > pixelbox.width && drawLabel.length() > 3) {
            drawLabel = drawLabel.substring(0, drawLabel.length() - 2) + "\u2026";
            textWidth = fm.stringWidth(drawLabel);
        }
        int textHeight = fm.getAscent();
        if (textWidth <= pixelbox.width && textHeight <= (labelGap + pixelbox.height / 2)) {
            int xpos = pixelbox.x + (pixelbox.width / 2) - (textWidth / 2);
            g2.setPaintMode(); //added to fix bug with collapsed or condensed track labels
            if (label_loc == NORTH) {
                g2.drawString(drawLabel, xpos, pixelbox.y + pixelbox.height / 2 - labelGap - 2);
            } else if (label_loc == SOUTH) {
                g2.drawString(drawLabel, xpos, pixelbox.y + pixelbox.height / 2 + textHeight + labelGap - 1);
            }
        }
    }

    /**
     * Overriding addChild to force a call to adjustChildren().
     */
    @Override
    public void addChild(GlyphI glyph) {
        if (isMoveChildren()) {
            double child_height = adjustChild(glyph);
            super.addChild(glyph);
            if (this.getCoordBox().height < 2.0 * child_height) {
                this.getCoordBox().height = 2.0 * child_height;
                adjustChildren();
            }
        } else {
            super.addChild(glyph);
        }
    }

    protected double adjustChild(GlyphI child) {
        if (isMoveChildren()) {
            // child.cbox.y is modified, but not child.cbox.height)
            // center the children of the LineContainerGlyph on the line
            final Rectangle2D.Double cbox = child.getCoordBox();
            double ycenter;
            // use moveAbsolute or moveRelative to make sure children also get moved

            if (show_label) {
                if (label_loc == NORTH) {
                    ycenter = this.getCoordBox().y + (0.75 * this.getCoordBox().height);
                    child.moveRelative(0, ycenter - cbox.height / 2 - cbox.y);
                } else {
                    ycenter = this.getCoordBox().y + (0.25 * this.getCoordBox().height);
                    child.moveRelative(0, ycenter - cbox.height / 2 - cbox.y);
                }
            } else {
                ycenter = this.getCoordBox().y + this.getCoordBox().height * 0.5;
            }
            child.moveRelative(0, ycenter - (cbox.height * 0.5) - cbox.y);
            return cbox.height;
        } else {
            return this.getCoordBox().height;
        }
    }

    protected void adjustChildren() {
        double max_height = 0.0;
        if (isMoveChildren()) {
            List<GlyphI> childlist = this.getChildren();
            if (childlist != null) {
                int child_count = this.getChildCount();
                for (int i = 0; i < child_count; i++) {
                    GlyphI child = childlist.get(i);
                    double child_height = adjustChild(child);
                    max_height = Math.max(max_height, child_height);
                }
            }
        }
        if (this.getCoordBox().height < 2.0 * max_height) {
            this.getCoordBox().height = 2.0 * max_height;
            adjustChildren(); // have to adjust children again after a height change.
        }
    }

    @Override
    public void pack(ViewI view) {
        if (isMoveChildren()) {
            this.adjustChildren();
            // Maybe now need to adjust size of total glyph to take into account
            // any expansion of the children ?
        } else {
            super.pack(view);
        }
    }

    @Override
    public void setLabelLocation(int loc) {
        if (loc != getLabelLocation()) {
            adjustChildren();
        }
        super.setLabelLocation(loc);
    }

    @Override
    public void setShowLabel(boolean b) {
        if (b != getShowLabel()) {
            adjustChildren();
        }
        super.setShowLabel(b);
    }

    /**
     * If true, {@link #addChild(GlyphI)} will automatically center the child
     * vertically.
     */
    public boolean isMoveChildren() {
        return this.move_children;
    }

    /**
     * Set whether {@link #addChild(GlyphI)} will automatically center the child
     * vertically.
     */
    public void setMoveChildren(boolean move_children) {
        this.move_children = move_children;
    }

    private void drawClassicLabel(int originalPixWidth, Rectangle pixelbox, Graphics g) {
        int xpix_per_char = originalPixWidth / label.length();
        int ypix_per_char = (pixelbox.height / 2 - pixelSeparation);
        // only draw if there is enough width for smallest font
        if ((xpix_per_char >= minCharXpix) && (ypix_per_char >= minCharYpix)) {
            if (xpix_per_char > maxCharXpix) {
                xpix_per_char = maxCharXpix;
            }
            if (ypix_per_char > maxCharYpix) {
                ypix_per_char = maxCharYpix;
            }
            Font xmax_font = xpix2fonts[xpix_per_char];
            Font ymax_font = ypix2fonts[ypix_per_char];
            Font chosen_font = (xmax_font.getSize() < ymax_font.getSize()) ? xmax_font : ymax_font;
            Graphics g2 = g;
            g2.setFont(chosen_font);
            FontMetrics fm = g2.getFontMetrics();

            int textWidth = fm.stringWidth(label);
            int textHeight = fm.getAscent(); // trying to fudge a little (since ascent isn't quite what I want)

            if (((textWidth <= pixelbox.width)) && (textHeight <= (pixelSeparation + pixelbox.height / 2))) {
                int xpos = pixelbox.x + (pixelbox.width / 2) - (textWidth / 2);
                g2.setPaintMode(); //added to fix bug with collapsed or condensed track labels
                if (label_loc == NORTH) {
                    g2.drawString(label, xpos,
                            //		       pixelbox.y + text_height);
                            pixelbox.y + pixelbox.height / 2 - pixelSeparation - 2);
                } else if (label_loc == SOUTH) {
                    g2.drawString(label, xpos,
                            pixelbox.y + pixelbox.height / 2 + textHeight + pixelSeparation - 1);
                }
            }
        }
    }
}
