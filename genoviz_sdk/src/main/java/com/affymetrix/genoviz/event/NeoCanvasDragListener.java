package com.affymetrix.genoviz.event;

import java.util.EventListener;

/**
 *
 * @author hiralv
 */
public interface NeoCanvasDragListener extends EventListener {

    public void canvasDragEvent(NeoCanvasDragEvent e);
}
